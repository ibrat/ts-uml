# tsuml
Генератор TypeScript 2+ кода на основании UML2.5 (XMI 2.5.1) и UML2.5 (XMI 2.5.1) по TypeScript 2+

# установка
$ npm i 

Возможно потребуется выполнить в powershell under admin:
$ npm install --global --production windows-build-tools


# 
Генерация TypeScript (классы, интерфейсы и юнит-тесты):
```node index uml2ts from=<путь к UML-файлу> to=<папка для TypeScript кода>```

Генерация UML:
```node index ts2uml from=<папка для TypeScript кода> to=<путь к UML-файлу>```
