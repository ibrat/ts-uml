const fs = require('fs');
const recursiveReaddir = require('recursive-readdir');
const uml = require('./uml');
const ts = require('./ts');

//const sourcePath = './source';
//const targetPath = './uml.xml';


if ( process.argv.some(function (value) { return value === 'ts2uml' }) ) {
	ts2uml(process.argv);
} else if ( process.argv.some(function (value) { return value === 'uml2ts' }) ) {
	uml2ts(process.argv);
} else {
	console.log('Неизвестная команда!');
}

function uml2ts(params) {
	var umlFilename = '';
	var tsDirectory = '';
	params.forEach(function (param) {
		if (param.search('from=') > -1) {
			umlFilename = param.replace('from=', '');
		} else if (param.search('to=') > -1) {
			tsDirectory = param.replace('to=', '');
		}
	});

	if ( ! umlFilename || ! tsDirectory) {
		console.log('Укажите from=<путь к UML-файлу> и to=<путь к папке с исходным кодом>');
		return;
	}

	console.log('Генерируется TypeScript-код в директории: ' + tsDirectory);

	fs.readFile(umlFilename, function (error, data) {
		if (error) {
			console.log('Ошибка чтения файла: ', error);
		} else {
			new ts(data.toString()).toFiles(tsDirectory);
		}
	});
}

function ts2uml(params) {

	var sourcePath = '';
	var targetPath = '';
	params.forEach(function (param) {
		if (param.search('from=') > -1) {
			sourcePath = param.replace('from=', '');
		} else if (param.search('to=') > -1) {
			targetPath = param.replace('to=', '');
		}
	});

	if ( ! sourcePath || ! targetPath) {
		console.log('Укажите from=<путь к UML-файлу> и to=<путь к папке с исходным кодом>');
		return;
	}

	console.log('Генерируется UML-документ...');

	recursiveReaddir(sourcePath, ['*.html', '*.md', '*.spec.ts', '*example.ts', '*.ts.orig'])
		.then(function (names) {
			const files = names.map(function (filename) {
				return fs.readFileSync(filename).toString();
			});

			fs.writeFile(targetPath, new uml(files).toXml(), 'utf8', function (error) {
				if (error) {
					console.log('Ошибка записи в файл', targetPath, error);
				} else {
					console.log('UML успешно сгенерирован в ', targetPath);
				}
			});
		})
		.catch(function (error) {
			console.log('ERROR: recursiveReaddir', error);
		});
}
