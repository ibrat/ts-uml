const parser = require('xml2json');
const fs = require('fs');
const toSlugCase = require('to-slug-case')
const handlebars = require('handlebars').create();

handlebars.registerHelper('ifNotLastIndex', function(index, arr, options) {
	if(index !== arr.length - 1) {
		return options.fn(this);
	}
	return options.inverse(this);
});

function ts(input) {

	const data = JSON.parse(parser.toJson(input));

	const templateType = {
		class: {
			type: 'model',
			folder: '',
			path: './templates/class.hbs'
		},
		spec: {
			type: 'spec',
			folder: '/specs',
			path: './templates/spec.hbs'
		},
		interface: {
			type: 'interface',
			folder: '/interfaces',
			path: './templates/interface.hbs',
		},
		enum: {
			type: 'enum',
			folder: '/interfaces',
			path: './templates/enum.hbs',
		}
	};

	const createClasses = function (data, dirname) {
		createFiles(data, dirname, 'uml:Class', templateType.class);
		createFiles(data, dirname, 'uml:Class', templateType.spec);
		createFiles(data, dirname, 'uml:Interface', templateType.interface);
		createEnums(data, dirname, 'uml:Enumeration', templateType.enum);
	};

	const createEnums = function (data, dirname, elementType, template) {
		const classes = searchNodesByAttr(data['xmi:XMI']['uml:Model'], 'xmi:type', elementType);
		const classTemplate = handlebars.compile(fs.readFileSync(template.path).toString());

		dirname = dirname + template.folder;
		if ( ! fs.existsSync(dirname)){
			fs.mkdirSync(dirname);
		}
		classes.forEach(function (file) {
			const model = {
				raw: file,
				name: file.name,
				values: file.ownedLiteral ?
					(Array.isArray(file.ownedLiteral) ? file.ownedLiteral : [file.ownedLiteral])
						.map(function (literal) {
							return {
								raw: literal,
								name: literal.name
							};
						})
					: []
			};
			model.fileName = toSlugCase(model.name);

			fs.writeFileSync(dirname + '/' + model.fileName + '.' + template.type + '.ts', classTemplate(model));
		});
	}

	const createFiles = function (data, dirname, elementType, template) {
		const classes = searchNodesByAttr(data['xmi:XMI']['uml:Model'], 'xmi:type', elementType);
		const classTemplate = handlebars.compile(fs.readFileSync(template.path).toString());

		dirname = dirname + template.folder;
		if ( ! fs.existsSync(dirname)){
			fs.mkdirSync(dirname);
		}
		classes.forEach(function (file) {


			const operations = file.ownedOperation ?
				(Array.isArray(file.ownedOperation) ? file.ownedOperation : [file.ownedOperation])
					.map(function (operation) {

						const params = (Array.isArray(operation.ownedParameter) ? operation.ownedParameter : [operation.ownedParameter])
							.map(function (param) {
								return {
									direction: param.direction,
									name: param.name,
									type: getTypeById(param.type)
								};
							});

						const operationType = params.filter(function (param) {
							return param.name === 'return' && param.direction === 'return';
						})[0];

						return {
							raw: operation,
							visibility: 'public',
							name: operation.name,
							type: operationType && operationType.type && getTypeById(operationType.type),
							params: params.filter(function (param) {
								return param.name !== 'return' && param.direction !== 'return';
							})
						};
					})
				: [];

			const model = {
				raw: file,
				name: file.name.replace(/[\.:\/]*/g, ''), // $help://bpmn_1_4.htm
				attributes: file.ownedAttribute ?
					(Array.isArray(file.ownedAttribute) ? file.ownedAttribute : [file.ownedAttribute])
						.map(function (attribute) {
							return {
								raw: attribute,
								visibility: attribute.visibility,
								name: attribute.name,
								type: getTypeById(typeof attribute.type === 'object' ? attribute.type['xmi:idref'] : attribute.type)
							};
						})
						.filter(function (attribute) {
							return !!attribute.name;
						})
					: [],
				operations: operations.filter(function (operation) {
					return operation.name !== file.name;
				}),
				construct: operations.filter(function (operation) {
					return operation.name === file.name;
				})[0],
				dependencies:[]
			};
			model.varName = model.name.toLowerCase();
			model.fileName = toSlugCase(model.name);
			if (template.type === 'interface') {
				model.fileName = model.fileName.replace(/^i-/, '');
			}

			model.dependencies = getDependencies(model);

			console.log(dirname + '/' + model.fileName + '.' + template.type + '.ts');
			fs.writeFileSync(dirname + '/' + model.fileName + '.' + template.type + '.ts', classTemplate(model));
		});
	};

	const getDependencies = function(model) {
		return model.attributes
			.map(function (attribute) {
				return attribute.type;
			})
			.concat(model.operations
				.map(function (operation) {
					return operation.params
						.map(function (param) {
							return param.type;
						})
						.concat([operation.type]);
				})
				.reduce(function (a, b) {
					return a.concat(b);
				}, [])
			)
			.concat(model.construct ? model.construct.params.map(function (param) { return param.type }) : [])
			.filter(function (type) {
				return type;
			})
			.map(function (type) {
				return type
					.replace('string', '')
					.replace('number', '')
					.replace('boolean', '')
					.replace('void', '')
					.replace('any', '')
					.replace('Object', '')
					.replace('[]', '')
					.replace('Array<', '')
					.replace('>', '')
					.replace('extends', '')
					.replace(/\s/g, '');
			})
			.filter(function (type, index, list) {
				return type && list.indexOf(type) === index;
			})
			.map(function (type) {
				return {
					name: type,
					path: './' + type + '.ts'
				};
			});
	};

	const getTypeById = function (id) {
		var type = id;
		if ( ! id) {
			type = 'any';
		} else if (typeof id === 'object' && id['xmi:type'] === 'uml:PrimitiveType') {
			type = id.href.substring(id.href.search('#') + 1).toLowerCase();
		} else if (id.search('EAID_') > -1) {
			t = searchNodesByAttr(data, 'xmi:id', id)[0];
			if (t) {
				type = t.name;
			}
		}

		type = type
			.replace('EAJava_', '')
			.replace('EAnone_', '')
			.replace('EAJavaScript_', '');

		if ([
				'String',
				'Number',
				'Boolean'
			].indexOf(type) > -1) {
			type = type.toLowerCase();
		}

		type = type
			.replace('dateTime', 'number')
			.replace('DateTime', 'number')
			.replace('Long', 'number');

		return type;
	};

	const searchNodesByAttr = function(json, attrName, attrValue) {
		var result = [];
		if (json && typeof json === 'object') {
			if ( Object.keys(json).some(function (key) { return key === attrName && json[key] === attrValue }) ) {
				result.push(json);
			} else {
				result = result.concat(Object.keys(json)
					.map(function (key) {
						return searchNodesByAttr(json[key], attrName, attrValue);
					})
					.reduce(function (a, b) {
						return a.concat(b);
					}, []));
			}
		} else {
			//
		}
		return result;
	};

	return {
		toFiles: function (dirname) {
			createClasses(data, dirname);
		}
	}
}

module.exports = ts;
