const parser = require('xml2json');

function uml(files) {

	const guid = function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '_' + s4() + '_' + s4() + '_' +
			s4() + '_' + s4() + s4() + s4();
	};

	const eaid = function() {
		return 'EAID_' + guid().toUpperCase();
	};

	const packageId = eaid();
	const diagramId = eaid();

	const definition = {
		'xmi:XMI': {
			'xmlns:uml': 'http://www.omg.org/spec/UML/20131001',
			'xmlns:xmi': 'http://www.omg.org/spec/XMI/20131001',
			'xmlns:umldi': 'http://www.omg.org/spec/UML/20131001/UMLDI',
			'xmlns:dc': 'http://www.omg.org/spec/UML/20131001/UMLDC',
			'xmi:Documentation': {
				'exporter': 'Enterprise Architect',
				'exporterVersion': '6.5'
			},
			'uml:Model': {
				'xmi:id': packageId,
				'xmi:type': 'uml:Model',
				'name': 'EA_Model',
				'packagedElement': [
					{
						'xmi:type': 'uml:Class',
						'name': 'Class1'
					},
					{
						'xmi:type': 'uml:Class',
						'name': 'Class2'
					}
				],
				'umldi:Diagram': {
					'xmi:type': 'umldi:UMLClassDiagram',
					'isFrame': 'false',
					'xmi:id': diagramId,
					'modelElement': packageId,
					'ownedElement': []
				}
			},
			'xmi:Extension': {
				'extender': 'Enterprise Architect',
				'extenderID': '6.5',
				'elements': {
					'element': [
						{
							'xmi:type': 'uml:Class',
							'name': 'Class1',
							'scope': 'public'
						},
						{
							'xmi:type': 'uml:Class',
							'name': 'Class2',
							'scope': 'public'
						}
					]
				},
				'diagrams': {
					'diagram': {
						'xmi:id': diagramId,
						'model': {
							'package': packageId,
							'owner': packageId
						},
						'properties': {
							'name': 'EA_Model',
							'type': 'Logical'
						}
					}
				},
				'connectors': {
					'connector': []
				}
			}
		}
	};

	const data = definition;

	/**
	 * Преобразование файла к сущностям
	 *
	 * @param {string} file
	 * @return result[]
	 */
	const parseFile = function (file) {
		const matches = file.match(/(export[^s].*)/gm);
		return matches ?
			matches
				.filter(function (declaration, index, declarations) {
					return declaration.search('const') < 0;
				})
				.map(function (declaration, index, declarations) {

				const fileSegment = file.substring(file.search(declaration), declarations[index + 1] && file.search(declarations[index + 1]));

				const result = {
					type: getType(fileSegment),
					name: getName(fileSegment),
					attributes: getAttributes(fileSegment),
					operations: getOperations(fileSegment),
					values: getValues(fileSegment),
					generalization: getGeneralization(fileSegment),
					realization: getRealization(fileSegment),
				};
				//const dependencies = getDependencies(file);
				//result.dependencies = filterDependencies(dependencies, result.generalization, result.realization);
				result.dependencies = [];

				return result;
			})
			:
			[];
	};

	const getDefinition = function(file) {
		return /export\s+(class|interface|enum)\s+(\w+)/.exec(file);
	}

	/**
	 * Получение Типа Сущности
	 *
	 * @param {string} file
	 * @return {string} class|interface|enum
	 */
	const getType = function (file) {
		const def = getDefinition(file);
		return def && def[1];
	};

	/**
	 * Получение Имени сущности
	 *
	 * @param {string} file
	 * @return {string}
	 */
	const getName = function (file) {
		const def = getDefinition(file);
		return def && def[2];
	};

	/**
	 * Получение Атрибутов сущости
	 *
	 * @param {string} file
	 * @return {Array<{ visibility: string, name: string, type: string }>}
	 */
	const getAttributes = function (file) {
		var attrs = file.match(/(private|protected|public)(\s+)?(readonly|static)?(\s+)?(\w+)[\s]*:[\s]*(\w+)/mg);
		if (attrs) {
			attrs = attrs.map(function (attr) {
				const values = attr
					.replace(/readonly/g, '')
					.replace(/static/g, '')
					.replace(/\W+/g, ' ')
					.split(' ');
				return {
					visibility: values[0],
					name: values[1],
					type: values[2]
				};
			});
		}

		return attrs || [];
	};

	/**
	 * Получение Методов сущности
	 *
	 * @param {string} file
	 * @return {Array<{ name: string, params: Array<{ name: string, type: string }>, type: string}>}
	 */
	const getOperations = function (file) {
		var operations = file.match(/(public)?\s+\w+(<[^>]+>)?\((.*)\)[\s]*:[\s]*(\S+)(\s+)?(\{|;)/mg) || [];

		if (operations.length > 0) {
			operations = operations.map(function(action) {
				const signature = action
					.replace('public', '')
					.replace('{', '')
					.replace(';', '')
					.replace(/\s+/g, '')
					.replace(/</g, '&lt;')
					.replace(/>/g, '&gt;');

				var params = signature.match(/\((.+)\)/);
				if (params) {
					params = params[1]
						.split(',')
						.map(function (value) {
							const values = value.split(':');
							return {
								name: values[0],
								type: values[1]
							}
						});
				}

				return {
					name: signature.substring(0, signature.search(/\(/)),
					params: params || [],
					type: signature.substring(signature.lastIndexOf(')') + 1).replace(/:/, '')
				}
			});
		}

		const constructMethod = /constructor\((.*)\)/gm.exec(file);
		if (constructMethod) {
			const constructParams = constructMethod[1].replace(/(private|protected|public)/g, '').replace(/\s+/g, ' ').split(',');

			const op = {
				name: getName(file),
				params: constructParams.map(function (param) {
					const values = param.split(':')
						.map(function (v) {
							return v && v.replace(/\s/g, '')
						});
					return {
						name: values[0],
						type: values[1]
					};
				}),
				type: ''
			};

			operations.push(op);
		}

		return operations;
	};

	const getValues = function (file) {
		const enumRegExp = /enum\s+\w+/;
		const enumDefinitionIndex = file.search(enumRegExp);
		var values = [];
		if (enumDefinitionIndex > -1) {
			values = file.substring(enumDefinitionIndex + enumRegExp.exec(file)[0].length).match(/[A-Z_]+/mg);
		}
		return values;
	};

	const getGeneralization = function (file) {
		const values = /export\s+(class|interface|enum)\s+(\w+)\s+extends\s+(\w+)/.exec(file);
		return values ? values[3] : null;
	};

	const getRealization = function (file) {
		const values = /implements\s+(\w+)/gm.exec(file);
		return values ? values[1] : null;
	};

	const getDependencies = function (file) {
		const dependenciesRegExp = /import(.*)from/gm;
		const dependencies = [];
		var matches = null;
		while (matches = dependenciesRegExp.exec(file)) {
			dependencies.push(matches[1].replace(/[\s\{\}]/g, ''));
		}
		return dependencies;
	};

	const filterDependencies = function (dependencies, generalization, realization) {
		return dependencies.filter(function (dep) {
			return dep !== generalization && dep !== realization;
		});
	};

	const toClassOrInterfacePackagedElement = function (entity) {
		return {
			'xmi:id': eaid(),
			'xmi:type': entity.type === 'class' ? 'uml:Class' : entity.type === 'interface' ? 'uml:Interface' : '',
			'name': entity.name,
			'isAbstract': entity.type === 'interface',
			'generalization': entity.generalization,
			'ownedAttribute': entity.attributes.map(function (attribute) {
				return {
					'xmi:type': 'uml:Property',
					'name': attribute.name,
					'visibility': attribute.visibility,
					'type': {
						'xmi:idref': 'EAJava_' + attribute.type
					}
				};
			}),
			'ownedOperation': entity.operations.map(function (operation) {
				return {
					'name': operation.name,
					'xmi:id': eaid(),
					'ownedParameter': operation.params.map(function (param) {
							return {
								'name': param.name,
								'type': 'EAJava_' + param.type
							};
						})
						.concat([{
							'name': 'return',
							'direction': 'return',
							'type': 'EAJava_' + operation.type
						}])
				};
			})
		};
	};

	const toClassOrInterfaceExtensionElement = function (element) {
		return {
			'xmi:idref': element['xmi:id'],
			'xmi:type': element['xmi:type'],
			'name': element.name,
			'scope': 'public',
			'operations': {
				'operation': element.ownedOperation.map(function (operation) {
					var returnType = operation.ownedParameter
						.filter(function (param) {
							return param.direction === 'return';
						});
					returnType = returnType[0] && returnType[0].type;
					returnType = returnType && returnType.substring(returnType.search('_') + 1);
					return {
						'xmi:idref': operation['xmi:id'],
						'type': {
							'type': returnType
						}
					};
				})
			}
		};
	};

	const toEnumPackagedElement = function (enumeration) {
		return {
			'xmi:id': eaid(),
			'name': enumeration.name,
			'xmi:type': 'uml:Enumeration',
			'ownedLiteral': enumeration.values.map(function (value) {
				return {
					'xmi:id': eaid(),
					'xmi:type': 'uml:EnumerationLiteral',
					'name': value
				}
			})
		}
	};

	const toEnumExtensionElement = function (enumeration) {
		return {
			'xmi:id': eaid(),
			'xmi:type': 'uml:Enumeration',
			'name': enumeration.name,
			'scope': 'public',
			'properties': {
				'isSpecification': 'false',
				'sType': 'Enumeration',
				'nType': '0',
				'scope': 'public',
				'isRoot': 'false',
				'isLeaf': 'false',
				'isAbstract': 'false'
			},
			'attributes': {
				'attribute': enumeration.values.map(function (value) {
					return {
						'name': value,
						'scope': 'Public',
						'stereotype': {
							'stereotype': 'enum'
						}
					};
				})
			}
		}
	};

	const parsedFiles = files
		.map(function (file) {
			return parseFile(file);
		})
		.reduce(function (filesA, filesB) {
			return filesA.concat(filesB);
		})
		.filter(function (entity) {
			return entity.type && entity.name;
		});

	const getElementIdByName = function (elements, elementName) {
		const element = getElementByName(elements, elementName);
		return element && element['xmi:id'] || '';
	};

	const getElementByName = function (elements, elementName) {
		var element = null;
		elements.some(function (entity) {
			if (entity.name === elementName) {
				element = entity;
			}
			return element;
		});
		return element || '';
	};

	const isPrimitiveType = function (typename) {
		return [
			'EAJava_number',
			'EAJava_boolean',
			'EAJava_string'
		].indexOf(typename) > -1;
	};

	const packagedElements = parsedFiles
		.map(function (entity) {
			return entity.type === 'enum' ? toEnumPackagedElement(entity) : toClassOrInterfacePackagedElement(entity);
		})
		.map(function (element, index, list) {
			if (element.generalization) {
				element.generalization = {
					'xmi:type': 'uml:Generalization',
					'xmi:id': eaid(),
					'general': getElementIdByName(list, element.generalization)
				};
			} else {
				delete element.generalization;
			}
			return element;
		});

	data['xmi:XMI']
		['uml:Model']
		['packagedElement'] = packagedElements
		.concat(parsedFiles
			.map(function (file) {
				return file.dependencies
					.map(function (dep) {
						const supplierId = getElementIdByName(packagedElements, dep);
						return supplierId ? {
							'xmi:type': 'uml:Usage',
							'xmi:id': eaid(),
							'supplier': supplierId,
							'client': getElementIdByName(packagedElements, file.name)
						} : null;
					})
					.filter(function (dep) {
						return dep;
					});
			})
			.reduce(function (depsA, depsB) {
				return depsA.concat(depsB);
			}, [])
		)
		.concat(parsedFiles
			.filter(function (file) {
				return file.realization;
			})
			.map(function (file) {
				return {
					'xmi:type': 'uml:Realization',
					'xmi:id': eaid(),
					'supplier': getElementIdByName(packagedElements, file.realization), // interface
					'client': getElementIdByName(packagedElements, file.name) // implementation
				}
			})
			.filter(function (element) {
				return element.supplier && element.supplier !== element.client;
			})
		)
		.concat(packagedElements
			.filter(function (element) {
				return element['xmi:type'] === 'uml:Class' || element['xmi:type'] === 'uml:Interface';
			})
			.map(function (element, index, elements) {
				const compositions = element.ownedAttribute
					.map(function (attr) {
						return ! isPrimitiveType(attr.type['xmi:idref']) &&
							getElementByName(elements, attr['type']['xmi:idref'].substring(attr['type']['xmi:idref'].search('_') + 1));
					})
					.filter(function (el, index, list) {
						return el && list.lastIndexOf(el) === index;
					})
					.map(function (nestedElement) {
						const associationId = eaid();
						const srcId = eaid();
						const dstId = eaid();

						nestedElement.ownedAttribute.push({
							'xmi:type': 'uml:Property',
							'xmi:id': dstId,
							'association': associationId,
							'type': {
								'xmi:idref': element['xmi:id']
							}
						});

						return {
							'xmi:type': 'uml:Association',
							'xmi:id': associationId,
							'memberEnd': [
								{
									'xmi:idref': srcId
								},
								{
									'xmi:idref': dstId
								}
							],
							'ownedEnd': {
								'xmi:type': 'uml:Property',
								'xmi:id': srcId,
								'association': associationId,
								'aggregation': 'composite',
								'type': {
									'xmi:idref': nestedElement['xmi:id']
								}
							}
						};
					});

				return compositions;
			})
			.reduce(function (elementsA, elementsB) {
				return elementsA.concat(elementsB);
			}, [])
		);

	data['xmi:XMI']
		['uml:Model']
		['umldi:Diagram']
		['ownedElement'] = packagedElements
			.map(function (element) {
				return {
					'xmi:type': 'umldi:UMLClassifierShape',
					'modelElement': element['xmi:id']
				};
			});

	data['xmi:XMI']
		['xmi:Extension']
		['elements']
		['element'] = packagedElements
		.filter(function (element) {
			return element['xmi:type'] === 'uml:Class' || element['xmi:type'] === 'uml:Interface';
		})
		.map(function (element) {
			return toClassOrInterfaceExtensionElement(element);
		});

	/*data['xmi:XMI']
		['xmi:Extension']
		['elements']
		['element'] = parsedFiles
		.filter(function (file) {
			return file.type === 'enum';
		})
		.map(function (entity) {
			return entity.type === 'enum' ? toEnumExtension(entity) : null;
		})
		.filter(function (entity) {
			return entity;
		});*/

	data['xmi:XMI']
		['xmi:Extension']
		['connectors']
		['connector'] = data['xmi:XMI']['uml:Model']['packagedElement']
		.filter(function (element) {
			return element['xmi:type'] === 'uml:Association';
		})
		.map(function (element) {
			return {
				'xmi:idref': element['xmi:id'],
				'properties': {
					'ea_type': 'Aggregation',
					'direction': 'Source -&gt; Destination'
				}
			};
		});
/*
	data['xmi:XMI']
		['thecustomprofile:enum'] = packagedElements
		.filter(function (element) {
			return element['xmi:type'] === 'uml:Enumeration';
		})
		.map(function (element) {
			return element.ownedLiteral;
		})
		.reduce(function (enumsA, enumsB) {
			return enumsA.concat(enumsB);
		}, [])
		.map(function (enumValue) {
			return {
				'base_EnumerationLiteral': enumValue['xmi:id']
			};
		});
*/
	return {
		toXml: function () {
			return parser.toXml(data);
		}
	}
}


module.exports = uml;
